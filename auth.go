package auth

import (
	"log"
	"net/http"
)

var (
	authProviders = make(map[string]AuthProvider)
)

type AuthProvider interface {
	Auth(r *http.Request) bool
}

func AuthFilter(next http.Handler, failStatus int) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("Process client auth")

		// Auth request with registered authenticators
		for _, a := range authProviders {
			if a.Auth(r) {
				next.ServeHTTP(w, r)
				return
			}
		}

		log.Println("Auth failed!")

		sendAuthFailed(w, failStatus)
	})
}

func Register(name string, authenticator AuthProvider) {
	if authenticator == nil {
		panic("Register authenticator is nil")
	}

	if _, ok := authProviders[name]; ok {
		panic("Register called twice for authenticator " + name)
	}

	authProviders[name] = authenticator
}

func sendAuthFailed(w http.ResponseWriter, failStatus int) {
	http.Error(w, http.StatusText(failStatus), failStatus)
}

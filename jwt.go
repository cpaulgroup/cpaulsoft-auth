package auth

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type JWTAuth struct {
	AuthHeader     string
	ExpirationTime time.Duration
	Secret         string
}

func (j *JWTAuth) Auth(r *http.Request) bool {
	tokenData := r.Header.Get(j.AuthHeader)
	if len(tokenData) > 0 {
		return validateToken(tokenData, j.Secret)
	}

	return false
}

func validateToken(tokenData string, secret string) bool {
	token, err := jwt.Parse(tokenData, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(secret), nil
	})

	if err != nil {
		log.Println("Token parse failed: %v", err)
		return false
	}

	if !token.Valid {
		log.Println("Token not valid: %v", token)
		return false
	}

	return true
}

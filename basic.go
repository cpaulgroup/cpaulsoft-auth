package auth

import (
	"encoding/base64"
	"net/http"
	"strings"
)

type BasicAuth struct {
	AuthHeader string
	Token      string
}

func (basic *BasicAuth) Auth(r *http.Request) bool {
	authHeader := r.Header.Get(basic.AuthHeader)
	if len(authHeader) > 0 {
		tokenBase64 := strings.TrimSpace(authHeader)
		return tokenBase64 == base64.StdEncoding.EncodeToString([]byte(basic.Token))
	}

	return false
}
